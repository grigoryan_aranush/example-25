﻿using System;
class Example
{
    static void Main()
    {

        int num;
        int factor;
        int i;
        bool isprime;

        for (num = 2; num < 20; num++)
        {
            isprime = true;
            factor = 0;
            for (i = 2; i <= num/2; i++)
            {
                if (num % i == 0)
                {
                    isprime = false;
                    factor = i;
                }
            }
            if (isprime)
            {
                Console.WriteLine("{0} is prime", num);

            }
            else
            {
                Console.WriteLine("Largest factor of {0} is {1}", num,factor);

            }
        }

    }
}